
<!-- Modal -->
<div class="modal" id="productModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal_cls_btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel">Get Information</h2>
            </div>
            <div class="modal-body">

              <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <?php $product = get_the_title(); ?>
                      <?php $collection = get_field('collection'); ?>
                      <?php gravity_form( 36, false, false, false, array('product' => $product, 'collection' => $collection), true ); ?>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">

                      <h4 class="modal-product_number"></h4>
                      <h5 class="modal-parent_family"></h5>
                      <div class="modal-brand"></div>

                      <!-- <div class="modal-parent_sku">Text</div> -->
                  </div>
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default modal_cls_btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function ($) {
        $('#productModalLink , .productModalLink').on('click', function (event) { 
            var button = $(this); 
            var brand = button.data('brand');
            var product_number = button.data('product_number');
            var title = button.data('title');
            var collection = button.data('collection');

            var modal = $(this)
           
            $('.modal-brand').text( brand);
            $('.modal-product_number').text(title + ' - ' + product_number);
            $('.modal-parent_family').text(collection);

            $('#productModal').addClass('fade');
            event.preventDefault();
        });

        $('.modal_cls_btn').on('click', function (event) {
            $('#productModal').removeClass('fade');
        });
    });
</script>